from flask import Flask, request, redirect
from flask.templating import render_template
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, migrate
import re


app = Flask(__name__)
app.debug = True
 
# adding configuration for using a sqlite database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'


# Creating an SQLAlchemy instance
db = SQLAlchemy(app)

# Settings for migrations
migrate = Migrate(app, db)
 
# Models
class Profile(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=False, nullable=False)
    email = db.Column(db.String(20), unique=False, nullable=False)
    ph_number = db.Column(db.Integer, nullable=False)
 
    # repr method represents  object will look like
    def __repr__(self):
        return f"Name : {self.name}, email:{self.email}, ph_number: {self.ph_number}"

# function to add profiles
@app.route('/add', methods=["POST"])
def add():
     
    #inside the get the name should
    # exactly be the same as that in the html
    # input fields
    p_name = request.form.get("name")
    p_email = request.form.get("email")
    p_ph_number = request.form.get("ph_number")
 
    # create an object of the Profile class of models
    # and store data as a row in our datatable
    if p_name != '' and p_email != '' and p_ph_number is not None and validation(p_email,p_name,p_ph_number)==True:
        p = Profile(name=p_name, email=p_email, ph_number=p_ph_number)
        db.session.add(p)
        db.session.commit()
        return redirect('/')
    else:
        return redirect('/')



#edit
@app.route('/edit/<int:id>', methods=["POST"])
def edit(id):
     
    # In this function we will input data from the
    # form pph_number and store it in our database.
    # Remember that inside the get the name should
    # exactly be the same as that in the html
    # input fields
    e_name = request.form.get("name")
    e_email = request.form.get("email")
    e_ph_number = request.form.get("ph_number")
 
    # create an object of the Profile class of models
    # and store data as a row in our datatable
    if e_name != '' and e_email != '' and e_ph_number is not None and validation(e_email,e_name,e_ph_number)==True:
        data = Profile.query.get(id)
        data.name=e_name
        data.email=e_email
        data.ph_number=e_ph_number
        db.session.commit()
        return redirect('/')
    else:
        return redirect('/')

#search

@app.route('/search', methods=["POST"])
def search():
     
    e_name = request.form.get("s_name")
    e_email = request.form.get("s_email")
 
    # create an object of the Profile class of models
  #  data = Profile()
  #  adta.name=e_name
  #  data.email=e_email
    data= Profile.query.filter_by(name=e_name , email=e_email).first()

    return render_template('search.html',search_a=data)


#delete route

@app.route('/delete/<int:id>')
def erase(id):
	# Deletes the data on the basis of unique id and
	# redirects to home
	data = Profile.query.get(id)
	db.session.delete(data)
	db.session.commit()
	return redirect('/')




#edit route
@app.route('/<int:eid>')
def edit_id(eid):
	# Deletes the data on the basis of unique id and
	# redirects to home pph_number
	data = Profile.query.get(eid)
    	
	return render_template('edit.html',edit_a=data)



#index route
@app.route('/')
def index():
	# Query all data and then pass it to the template
	profiles = Profile.query.all()
	return render_template('index.html', profiles=profiles)

#add route
@app.route('/add_data')
def add_data():
	return render_template('add_profile.html')


#validation function

def validation(test_email,test_name,test_ph_number):

    email_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    name_regex = r'\b[A-Za-z ]+[A-Za-z]\b' 
    num_regex=r"^\d{10}$"
    if(re.fullmatch(email_regex,test_email) and re.fullmatch(name_regex,test_name) and re.fullmatch(num_regex,test_ph_number)):
        return True
    else:
        return False
      
#def name_validation(test_name):
  #  email_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
 #   name_regex = r'\b[A-Za-z ]+[A-Za-z]\b'
  #  if(re.fullmatch(name_regex,test_name)):
   #     return True
    ##   return False



#app run
if __name__ == '__main__':
	app.run()
